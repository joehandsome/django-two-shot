from django.shortcuts import render, get_object_or_404, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm

# Create your views here.
@login_required
def home(request):
    receipts = Receipt.objects.filter(purchaser = request.user)
    context = {
        'receipts' : receipts
    }
    return render(request, 'receipt/home.html', context)

@login_required
def add_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit = False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = ReceiptForm()
    context = {
        'form' : form
    }
    return render(request, 'receipt/create.html', context)

@login_required
def view_categories(request):
    categories = ExpenseCategory.objects.filter(owner = request.user)
    context = {
        'categories' : categories,
    }
    return render(request, 'categories/list.html', context)

@login_required
def view_accounts(request):
    accounts = Account.objects.filter(owner = request.user)
    context = {
        'accounts' : accounts,
    }
    return render(request, 'accounts/list.html', context)

@login_required
def add_cat(request):
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid:
            category = form.save(commit = False)
            category.owner = request.user
            category.save()
            return redirect('category_list')
    else:
        form = CategoryForm()
    context = {
        'form' : form,
    }
    return render(request, 'categories/add.html', context)

@login_required
def add_acc(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit = False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = AccountForm
    context = {
        'form' : form,
    }
    return render(request, 'accounts/add.html', context)
