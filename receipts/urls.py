from django.urls import path
from receipts.views import home, add_receipt, view_categories, view_accounts, add_cat, add_acc

urlpatterns = [
    path('', home, name = 'home'),
    path('create/', add_receipt, name = 'create_receipt'),
    path('categories/', view_categories, name = 'category_list'),
    path('accounts/', view_accounts, name = 'account_list'),
    path('categories/create/', add_cat, name = 'create_category'),
    path('accounts/create/', add_acc, name = 'create_account'),
]
